var form= document.getElementById("form");
var inputs = document.querySelectorAll('#form input');
var alerta =document.getElementById("alerta");
var alerta2 =document.getElementById("resultado");

const EXPR = {
    numerico : /([0-9])/, texto : /([a-zA-Z])/, caracteres : /^[^a-zA-Z\d\s]+$/, espacios : /\s/g
}

inputs.forEach(input=>{
    input.addEventListener('keyup', (e) => {   
        let valueInput = e.target.value;
        switch(e.target.id){
            case 'num1':
                input.value = valueInput.replace(EXPR.texto, '').replace(EXPR.espacios, '');
            break;
            case 'num2':
                input.value = valueInput.replace(EXPR.texto, '').replace(EXPR.espacios, '');        
            break;
        }
    })
});

form.addEventListener("submit", e=>{
    e.preventDefault();
    var num1 =document.getElementById("num1");
    var num2 =document.getElementById("num2");
    var suma=0, i=0;
    if (num1.value <= num2.value) {
        for (i = num1.value; i<= num2.value; i++) {
                suma= parseInt(suma+i);
        }
        alerta.innerHTML='';
        alerta2.innerHTML = 'La suma es ' + suma;
    }else{
        alerta.innerHTML = 'El numero b debe ser mayor';
        alerta2.innerHTML='';
    }
})
